# Generated by Django 4.2.7 on 2023-11-09 08:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movie_app', '0002_requestcountermodel'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='moviemodel',
            index=models.Index(fields=['uuid'], name='movies_tabl_uuid_0af745_idx'),
        ),
        migrations.AlterModelTable(
            name='collectionmodel',
            table='collection_table',
        ),
        migrations.AlterModelTable(
            name='moviemodel',
            table='movies_table',
        ),
    ]
