from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from movie_app.services.user import UserService

userservice = UserService()

class UserRegistrationView(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        result = userservice.sign_up(request)
        return Response(result, status=result["code"])