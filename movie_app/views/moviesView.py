from rest_framework.views import APIView
from rest_framework.response import Response
from movie_app.services.movies import MovieService

movieservice = MovieService()

class GetAllMoviesListFromThirdPartyUrl(APIView):
    def get(self, request):
        result = movieservice.all_movies_list(request)
        return Response(result, status=result["code"])
    
class CollectionView(APIView):
    def post(self, request):
        result = movieservice.create_collection_for_user(request)
        return Response(result, status=result["code"])
    
    def get(self, request):
        result = movieservice.get_collections_of_user(request)
        return Response(result, status=result["code"])
    
class GetUpdateDeleteCollectionOfUserByUuidView(APIView):
    def get(self, request, collection_uuid):
        result = movieservice.get_collection_by_uuid(request, collection_uuid)
        return Response(result, status=result["code"])
    
    def delete(self, request, collection_uuid):
        result = movieservice.delete_collection_by_uuid(request, collection_uuid)
        return Response(result, status=result["code"])
    
    def put(self, request, collection_uuid):
        result = movieservice.update_collection_by_uuid(request, collection_uuid)
        return Response(result, status=result["code"])
    
class GetRequestsCountView(APIView):
    def get(self, request):
        result = movieservice.get_requests_count_for_server(request)
        return Response(result, status=result["code"])
    
class ResetRequestsCounterView(APIView):
    def get(self, request):
        result = movieservice.reset_requests_counter(request)
        return Response(result, status=result["code"])