from rest_framework import serializers
from movie_app.models import CollectionModel, MovieModel

class CollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectionModel
        fields = ['title', 'description', 'uuid']
        
class CollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectionModel
        fields = ['title','uuid', 'description']

class MoviesCollectionSerializer(serializers.ModelSerializer):
    movies = serializers.SerializerMethodField()
    class Meta:
        model = CollectionModel
        fields = ['title', 'description', 'movies']
    def get_movies(self, obj):
        try:
            movies = MovieModel.objects.filter(collection = obj.uuid)
            return movies.values()
        except:
            return None