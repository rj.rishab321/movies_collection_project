from .movieBaseService import MoviewsBaseService
from rest_framework import status 
import requests
from movie_project.settings import third_party_username, third_party_url, third_party_password
from utils.messages import *
import uuid
from movie_app.models import CollectionModel
from movie_app.serializers.collection import CollectionSerializer, MoviesCollectionSerializer
from movie_app.models import MovieModel, RequestCounterModel

class MovieService(MoviewsBaseService):

    def all_movies_list(self, request):
        url = third_party_url
        username = third_party_username
        password = third_party_password
        session = requests.Session()
        session.auth = (username, password)
        response = session.get(url, verify=False)
        if response.status_code == 200:
            return {"data": response.json(), "message": MOVIES_FETCHED, "code": status.HTTP_200_OK}
        else:
            return {"data": response.json(), "message": SOMETHING_WENT_WRONG, "code": status.HTTP_400_BAD_REQUEST}
        
    def create_collection_for_user(self, request):
        try:
            data = {
                "title" : request.data.get("title"), 
                "description" : request.data.get("description"), 
                "movies" : request.data.get("movies"), 
            }
            create_collection = CollectionModel.objects.create(
                title = data["title"],
                user_id = request.user.id,
                description = data["description"],
                uuid = uuid.uuid4(),
            )
            for movie in data["movies"]:
                add_movie_in_collection = MovieModel.objects.create(
                    title = movie["title"],
                    description = movie["description"],
                    genres = movie["genres"],
                    collection = create_collection,
                    uuid = movie["uuid"],
                )
            return {"data": {"collection_uuid": create_collection.uuid}, "message": COLLECTION_CREATED, "code": status.HTTP_201_CREATED}    
        except Exception as error:
            return {"data": str(error), "message": SOMETHING_WENT_WRONG, "code": status.HTTP_400_BAD_REQUEST}
        
    def get_collections_of_user(self, request):
        collections_of_user = CollectionModel.objects.filter(user_id=request.user.id)
        top_favourite_genres = self.get_top_favourite_genres(collections_of_user.values_list('uuid'))
        serializer = CollectionSerializer(collections_of_user, many=True)
        return {"is_success":True, "data":{"collections": serializer.data}, "favourite_genres": top_favourite_genres, "code": status.HTTP_200_OK}

    def get_top_favourite_genres(self, uuid):
        uuids = [i[0] for i in uuid]
        all_movie_genres = MovieModel.objects.filter(collection__in=uuids).values_list('genres', flat=True).distinct()[:3]
        print(all_movie_genres)
        return all_movie_genres
    
    def get_collection_by_uuid(self, request, collection_uuid):
        try:
            get_collection = CollectionModel.objects.get(uuid=collection_uuid)
        except:
            return {"data": None, "message": RECORD_NOT_FOUND, "code": status.HTTP_400_BAD_REQUEST}
        serializer = MoviesCollectionSerializer(get_collection)
        return {"data": serializer.data, "message": COLLECTION_FETCHED, "code": status.HTTP_200_OK}    
    
    def delete_collection_by_uuid(self, request, collection_uuid):
        try:
            get_collection = CollectionModel.objects.get(uuid=collection_uuid)
        except:
            return {"data": None, "message": RECORD_NOT_FOUND, "code": status.HTTP_400_BAD_REQUEST}
        get_collection.delete()    
        return {"data": None, "message": COLLECTION_DELETED, "code": status.HTTP_200_OK}    
    
    def update_collection_by_uuid(self, request, collection_uuid):
        data = {
                "title" : request.data.get("title"), 
                "description" : request.data.get("description"), 
            }
        movies = request.data.get("movies")
        try:
            get_collection = CollectionModel.objects.get(uuid=collection_uuid)
        except:
            return {"data": None, "message": RECORD_NOT_FOUND, "code": status.HTTP_400_BAD_REQUEST}
        if movies is not None:
            for movie in movies:
                check_movie_existence = MovieModel.objects.filter(uuid = movie["uuid"], collection = collection_uuid)
                if not check_movie_existence:
                    add_movie_in_collection = MovieModel.objects.create(
                        title = movie["title"],
                        description = movie["description"],
                        genres = movie["genres"],
                        collection = get_collection,
                        uuid = movie["uuid"],
                    )
        serializer = CollectionSerializer(get_collection, data)
        if serializer.is_valid():
            serializer.save()
            return {"data": None, "message": COLLECTION_UPDATED, "code": status.HTTP_200_OK}
        return {"data": None, "message": SOMETHING_WENT_WRONG, "code": status.HTTP_400_BAD_REQUEST}
    
    def get_requests_count_for_server(self, request):
        count_requests = RequestCounterModel.objects.first()
        if count_requests is None:
            count_requests = 0
        else:
            count_requests = count_requests.count
        return {"requests": count_requests, "message": REQUESTS_COUNT, "code": status.HTTP_200_OK}    
    
    def reset_requests_counter(self, request):
        count_requests = RequestCounterModel.objects.first()
        if count_requests is not None:
            count_requests.count = 0
            count_requests.save()
        return {"data": None, "message": RESET_REQUESTS_COUNT, "code": status.HTTP_200_OK}