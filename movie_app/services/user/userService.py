from .userBaseService import UserBaseService
from movie_app.models import UserModel
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status 

class UserService(UserBaseService):
    def sign_up(self, request):
        data = {
            "username": request.data['username'],
            "password": request.data['password']
        }
        try:
            user = UserModel.objects.get(username=data['username'])
            if not user.check_password(data['password']):
                return {data:None, "message": "Incorrect Password", "code": status.HTTP_401_UNAUTHORIZED}
        except:
            user = UserModel.objects.create(username=data['username'])
            user.set_password(data['password'])
            user.save()
            
        token =RefreshToken.for_user(user)
        access_token = str(token.access_token)
        return {"access_token": access_token, "code": status.HTTP_201_CREATED}