from django.db import models
from movie_app.models import CollectionModel
import uuid

class MovieModel(models.Model):
    uuid = models.CharField(max_length=255, null=True, blank=True)
    collection = models.ForeignKey(CollectionModel, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    genres = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'movies_table'
        indexes = [
            models.Index(fields=['uuid'])
        ]