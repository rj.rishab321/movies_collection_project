from django.db import models
import uuid
from movie_app.models import UserModel

class CollectionModel(models.Model):
    user = models.ForeignKey(UserModel, on_delete=models.CASCADE, related_name="user")
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)    

    class Meta:
        db_table = 'collection_table'