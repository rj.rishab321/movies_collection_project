from django.db import models

class RequestCounterModel(models.Model):
    count = models.IntegerField(null=True, blank=True)