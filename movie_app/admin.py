from django.contrib import admin
from movie_app.models import *
# Register your models here.

admin.site.register(CollectionModel)
admin.site.register(MovieModel)
admin.site.register(RequestCounterModel)
admin.site.register(UserModel)