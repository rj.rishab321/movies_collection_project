from django.urls import path
from .views import *


urlpatterns = [
    path('movies/', GetAllMoviesListFromThirdPartyUrl.as_view(), name='movies'),
    path('register/', UserRegistrationView.as_view(), name='registration'),
    path('collection/', CollectionView.as_view(), name='collection'),
    path('collection/<str:collection_uuid>/', GetUpdateDeleteCollectionOfUserByUuidView.as_view(), name='collection-operations'),

    #requests counter
    path('request-count/', GetRequestsCountView.as_view(), name='get-request-count'),
    path('request-count/reset/', ResetRequestsCounterView.as_view(), name='reset-request-count')
]