# middleware for counting requests
from movie_app.models import RequestCounterModel

class RequestsCounterMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        requests_counter = RequestCounterModel.objects.first()
        if requests_counter is None:
            RequestCounterModel.objects.create(count=1)
        else:
            requests_counter.count += 1
            requests_counter.save()
        response = self.get_response(request)
        return response